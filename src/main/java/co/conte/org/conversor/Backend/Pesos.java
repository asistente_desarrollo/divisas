package co.conte.org.conversor.Backend;

public class Pesos extends Divisa {

    public Pesos(double tasaConversion, double valor) {
        super(tasaConversion, valor);
    }

    @Override
    public double convertir() {
        return this.TasaConversion*this.valor;
    }
}
