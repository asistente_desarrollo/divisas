package co.conte.org.conversor.Backend;

public abstract class Divisa {

    public double TasaConversion;
    public double valor;

    public Divisa(double tasaConversion, double valor) {
        this.TasaConversion = tasaConversion;
        this.valor = valor;
    }

    public abstract double convertir();
}
