package co.conte.org.conversor.Backend;

public enum Tasas {

    dolares(0.00032),

    pesos(4147.3)
    ;
    double money;

    Tasas(double money) {
        this.money = money;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
