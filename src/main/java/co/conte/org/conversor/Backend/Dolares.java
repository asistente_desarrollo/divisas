package co.conte.org.conversor.Backend;

public class Dolares extends Divisa {

    public Dolares(double tasaConversion,double valor) {
        super(tasaConversion,valor);
    }

    @Override
    public double convertir() {
        return this.TasaConversion*valor;
    }



}
