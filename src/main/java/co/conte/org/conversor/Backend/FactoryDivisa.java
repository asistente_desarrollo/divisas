package co.conte.org.conversor.Backend;

public class FactoryDivisa {

    public static Divisa getDivisa(String className,double valor){
        Divisa divisa = null;
        if (Dolares.class.getSimpleName().equals(className))
            divisa = new Dolares(Tasas.dolares.getMoney(),valor);
        else if(Pesos.class.getSimpleName().equals(className))
            divisa = new Pesos(Tasas.pesos.getMoney(),valor);
        return divisa;
    }
}
