package co.conte.org.conversor;

import co.conte.org.conversor.Backend.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class ConversorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConversorApplication.class, args);
        Scanner teclado = new Scanner(System.in);
        String opcionDivisa;
        String divisa;
        String otherDivisa;
        //DecimalFormat df = new DecimalFormat("#.000");
        while(true) {
            System.out.println("1 -->  De pesos a dolares ");
            System.out.println("2 -->  De dolares a pesos ");
            opcionDivisa= teclado.nextLine();

            if (ConversorApplication.isNumeric(opcionDivisa) & (opcionDivisa.contains("1") | opcionDivisa.contains("2")))
                break;
            else
                System.out.println("Ingrese una opción valida");
        }
        if(opcionDivisa.contains("1")) {
            divisa = Pesos.class.getSimpleName();
            otherDivisa = Dolares.class.getSimpleName();
        }
        else {
            divisa = Dolares.class.getSimpleName();
            otherDivisa = Pesos.class.getSimpleName();
        }
        String in = "";
        while(true) {
            System.out.println("Ingrese "+divisa);
            while (true) {
                in = teclado.nextLine();
                if (ConversorApplication.isNumeric(in) || in.contains("exit"))
                    break;
                else
                    System.out.println("Ingrese un valor valido");
            }
            if (in.contains("exit"))
                break;


            Divisa div = FactoryDivisa.getDivisa(otherDivisa, Double.parseDouble(in));
            System.out.println("--> " + div.valor + " "+ divisa);
            System.out.println("--> " + div.TasaConversion + " Tasa de conversión");
            System.out.println("--> " + div.convertir() +" "+ otherDivisa);
        }


    }
    private static boolean isNumeric(String cadena){
        try {
            Double.parseDouble(cadena);
            return true;
        } catch (NumberFormatException nfe){
            return false;
        }
    }

}

